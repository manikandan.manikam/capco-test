# capco-test
Here I have used redux to maintain the store but this not required for this simple application but to show case my skill i used redux here.

I have added dropdown to achieve filter functionality and added button to fetch next page data.

## How to run the application
Run following commands
`npm install`
`npm start`
Then application will launch in url `http://localhost:9002/`
