import { FETCH_PAYMENTS, UPDATE_PAYMENTS, SET_FILTER, SET_ERROR } from './actions';

export const paymentReducer = (state = {
    fetching: false,
    payments: [],
    metaData: {},
    filterKey: '',
    error: ''
}, action) => {
    const { type, payload } = action;
    switch (type) {
        case FETCH_PAYMENTS:
            return { ...state, fetching: true };
        case UPDATE_PAYMENTS:
            return { ...state, payments: payload.results, metaData: payload.metaDatal, fetching: false };
        case SET_FILTER:
            return { ...state, filterKey: payload };
        case SET_ERROR:
            return { ...state, error: payload };
        default:
            return state;
    }
};
