import axios from 'axios';
export const FETCH_PAYMENTS = 'FETCH_PAYMENTS';
export const UPDATE_PAYMENTS = 'UPDATE_PAYMENTS';
export const SET_FILTER = 'SET_FILTER';
export const SET_ERROR = 'SET_ERROR';

export const getPayments = (pageIndex = '') => {
    return async (dispatch) => {
        dispatch({ type: FETCH_PAYMENTS });
        try {
            const response = await axios.get('http://localhost:9001/api/payments' + (pageIndex ? '?pagelndex=' + pageIndex : ''));
            dispatch({
                type: UPDATE_PAYMENTS,
                payload: response.data
            });
        } catch (error) {
            dispatch({
                type: SET_ERROR,
                payload: error.message
            });
        }
    };
};

export const updatePayments = (response) => {
    return {
        type: UPDATE_PAYMENTS,
        payload: response
    };
};

export const setFilter = (value) => {
    return {
        type: SET_FILTER,
        payload: value
    };
};
