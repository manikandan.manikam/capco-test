import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { paymentReducer } from './reducer';

export const store = createStore(paymentReducer, applyMiddleware(ReduxThunk));
