import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getPayments, setFilter } from './redux/actions';

export default function PaymentList() {
    const dispatch = useDispatch();
    const { fetching, payments, metaData, filterKey } = useSelector(state => state);
    useEffect(() => {
        dispatch(getPayments());
    }, []);
    const fetchNextPage = () => {
        dispatch(getPayments(metaData.nextPageIndex));
    };
    const filterHandler = (key) => {
        dispatch(setFilter(key));
    };
    const filteredItems = filterKey ? payments.filter(item => item.paymentStatus === filterKey) : payments;
    return (
        <div>
            <div>
                <div style={{ float: 'right', margin: '20px' }}>
                    {metaData.hasMoreElements && <button onClick={fetchNextPage} className="btn btn-primary" disabled={fetching}>Next Page</button>}
                </div>
                <div className="form-group">
                    <label>Payment Status</label>
                    <select className="form-control" style={{ width: "200px" }} onChange={(e) => filterHandler(e.target.value)} value={filterKey}>
                        <option value="">All</option>
                        <option value="A">Approved</option>
                        <option value="C">Cancelled</option>
                        <option value="P">Pending</option>
                    </select>
                    {filterKey && <label>Shown {filteredItems.length} out of {payments.length} </label>}
                </div>
            </div>
            <table className="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th rowSpan="2">#</th>
                        <th rowSpan="2">Amount</th>
                        <th rowSpan="2">Currency</th>
                        <th rowSpan="2">Type</th>
                        <th rowSpan="2">Date</th>
                        <th rowSpan="2">Status</th>
                        <th colSpan="3" style={{ textAlign: 'center' }}>From Account</th>
                        <th colSpan="3" style={{ textAlign: 'center' }}>To Account</th>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Number</th>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Number</th>
                    </tr>
                </thead>
                <tbody>
                    {fetching ? <tr><td>Loading...</td></tr> : filteredItems.map((item, idx) => {
                        return (
                            <tr key={idx}>
                                <td>{idx + 1}</td>
                                <td>{item.paymentAmount}</td>
                                <td>{item.paymentCurrency}</td>
                                <td>{item.paymentType}</td>
                                <td>{item.paymentDate}</td>
                                <td>{item.paymentStatus}</td>
                                <td>{item.toAccaunt.accountName}</td>
                                <td>{item.toAccaunt.sortCode}</td>
                                <td>{item.toAccaunt.accountNumber}</td>
                                <td>{item.fromAccount.accountName}</td>
                                <td>{item.fromAccount.sortCode}</td>
                                <td>{item.fromAccount.accountNumber}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
