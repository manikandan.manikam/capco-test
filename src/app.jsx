import React from "react";
import { Provider } from 'react-redux';
import { store } from './redux/store';

import "bootstrap/dist/css/bootstrap.min.css";
import "./app.scss";
import PaymentList from "./PaymentList";

const App = () => {
  return (
    <Provider store={store}>
      <PaymentList />
    </Provider>
  );
};

export default App;
